//============================================================================
// Name        : basic_drawing_principles.cpp
// Author      : vito.Ombero
// Version     :
// Copyright   : Your copyright notice
// Description : primitive render testcase, Ansi-style
//============================================================================
#include <iostream>

#include "/home/vito/eclipseCosmos/cppOpenGlws/basic_drawing_principles/include/gshaders.hxx"
#include "/home/vito/eclipseCosmos/cppOpenGlws/basic_drawing_principles/include/imgfile.hxx"
#include "/home/vito/eclipseCosmos/cppOpenGlws/basic_drawing_principles/include/pixel.hxx"
#include "/home/vito/eclipseCosmos/cppOpenGlws/basic_drawing_principles/include/primitiverender.h"
#include "/home/vito/eclipseCosmos/cppOpenGlws/basic_drawing_principles/include/vshaders.hxx"

int main()
{
    using namespace vitoBasicDrawing;

    constexpr size_t width       = 200;
    constexpr size_t height      = 200;
    const size_t     buffer_size = (width - 1) * (height - 1);

    primitive_render P;

    auto dt = dot({ 20, 20 }, color::DarkOrange1B);

    P.createPixelBuffer(height, width);

    P.updatePixelBuffer(dt);

    std::array<vitoBasicDrawing::pixel, buffer_size> image;

    writePixelBufferToImage(P, image);
    std::cout << "writePixelBufferToImage(P, image); -- ok" << std::endl;

    imgFile f("01_try01_dot.ppx");
    f.save_p6(image, width, height);
    std::cout << "try01_dot.ppx is ready" << std::endl;

    //***********************************************************//

    auto ln1 = line({ 100, 100 }, { 150, 150 }, color::Red);
    P.updatePixelBuffer(ln1);
    auto ln3 = line({ 100, 100 }, { 150, 100 }, color::Yellow);
    P.updatePixelBuffer(ln3);
    auto ln6 = line({ 100, 100 }, { 150, 50 }, color::Indigo);
    P.updatePixelBuffer(ln6);
    auto ln2 = line({ 100, 100 }, { 100, 150 }, color::Orange);
    P.updatePixelBuffer(ln2);
    auto ln5 = line({ 100, 100 }, { 100, 50 }, color::Blue);
    P.updatePixelBuffer(ln5);
    auto ln7 = line({ 100, 100 }, { 50, 150 }, color::Purple);
    P.updatePixelBuffer(ln7);
    auto ln4 = line({ 100, 100 }, { 50, 100 }, color::Green);
    P.updatePixelBuffer(ln4);
    auto ln8 = line({ 100, 100 }, { 50, 50 }, color::Gray);
    P.updatePixelBuffer(ln8);
    auto ln = line({ 100, 100 }, { 100, 100 }, color::Black);
    P.updatePixelBuffer(ln);

    auto ln9 = line({ 0, 0 }, { 190, 13 }, color::Black);
    P.updatePixelBuffer(ln9);
    auto ln10 = line({ 190, 14 }, { 0, 1 }, color::Orange);
    P.updatePixelBuffer(ln10);

    writePixelBufferToImage(P, image);

    imgFile f12("02_try_line_full_test1.ppx");
    f12.save_p6(image, width, height);
    std::cout << "try_line_full_test1 is ready" << std::endl;

    //***********************************************************//

    P.deletePixelBuffer();
    P.createPixelBuffer(height, width);

    auto lin1 = line({ 87, 87 }, { 177, 177 }, color::Red);
    P.updatePixelBuffer(lin1);
    auto lin3 = line({ 87, 87 }, { 177, 87 }, color::Yellow);
    P.updatePixelBuffer(lin3);
    auto lin6 = line({ 87, 87 }, { 177, 27 }, color::Indigo);
    P.updatePixelBuffer(lin6);
    auto lin2 = line({ 87, 87 }, { 87, 177 }, color::Orange);
    P.updatePixelBuffer(lin2);
    auto lin5 = line({ 87, 87 }, { 87, 27 }, color::Blue);
    P.updatePixelBuffer(lin5);
    auto lin7 = line({ 87, 87 }, { 27, 177 }, color::Purple);
    P.updatePixelBuffer(lin7);
    auto lin4 = line({ 87, 87 }, { 27, 87 }, color::Green);
    P.updatePixelBuffer(lin4);
    auto lin8 = line({ 87, 87 }, { 27, 27 }, color::Gray);
    P.updatePixelBuffer(lin8);
    auto lin = line({ 87, 87 }, { 87, 87 }, color::Black);
    P.updatePixelBuffer(lin);

    auto lin9 = line({ 0, 0 }, { 190, 13 }, color::Black);
    P.updatePixelBuffer(lin9);
    auto lin10 = line({ 190, 14 }, { 0, 1 }, color::Orange);
    P.updatePixelBuffer(lin10);

    auto lin11 = line({ 0, 13 }, { 190, 0 }, color::Green_springB);
    P.updatePixelBuffer(lin11);
    auto lin12 = line({ 190, 1 }, { 0, 14 }, color::Khaki1_l);
    P.updatePixelBuffer(lin12);

    auto lin13 = line({ 0, 0 }, { 13, 190 }, color::Lime);
    P.updatePixelBuffer(lin13);
    auto lin14 = line({ 14, 190 }, { 1, 0 }, color::Red);
    P.updatePixelBuffer(lin14);

    auto lin15 = line({ 13, 0 }, { 0, 190 }, color::Cyan);
    P.updatePixelBuffer(lin15);
    auto lin16 = line({ 1, 190 }, { 14, 0 }, color::Teal);
    P.updatePixelBuffer(lin16);

    writePixelBufferToImage(P, image);

    imgFile f13("03_try_line_full_test2.ppx");
    f13.save_p6(image, width, height);
    std::cout << "try_line_full_test2 is ready" << std::endl;

    //***********************************************************//

    P.deletePixelBuffer();
    P.createPixelBuffer(height, width);

    auto tr1 = triangle({ 170, 170 }, { 10, 170 }, { 25, 10 }, color::Navy);
    P.updatePixelBuffer(tr1);

    writePixelBufferToImage(P, image);

    imgFile f20("04_try_z_triangle.ppx");
    f20.save_p6(image, width, height);
    std::cout << "try_z_triangle.ppx is ready" << std::endl;

    //***********************************************************//

    P.deletePixelBuffer();
    P.createPixelBuffer(height, width);

    auto c1 = circle({ 100, 100 }, { 50, 50 }, color::Navy);
    P.updatePixelBuffer(c1);

    writePixelBufferToImage(P, image);

    imgFile f30("05_try_za_circle.ppx");
    f30.save_p6(image, width, height);
    std::cout << "ttry_za_circle.ppx is ready" << std::endl;

    //***********************************************************//

    P.deletePixelBuffer();
    P.createPixelBuffer(height, width);

    auto c2 = circle({ 100, 100 }, { 50, 50 }, color::Red);
    P.applyShader(shaders::primitive::invertColor, c2);
    P.updatePixelBuffer(c2);

    writePixelBufferToImage(P, image);

    imgFile f40("06_try_za_shader_01_primitiv_Red_circle.ppx");
    f40.save_p6(image, width, height);
    std::cout << "try_za_shader_01_primitiv.ppx is ready" << std::endl;

    //***********************************************************//

    P.deletePixelBuffer();
    P.createPixelBuffer(height, width);

    auto tr2 = triangle({ 10, 170 }, { 25, 10 }, { 170, 170 }, color::Navy);
    P.updatePixelBuffer(tr2);

    auto v = new std::vector<pvertex>;
    v->push_back({ 10, 170 });
    v->push_back({ 25, 10 });
    v->push_back({ 170, 170 });
    P.applyShader(shaders::vertex::fill3border, *v, P.getW(), P.getH());

    writePixelBufferToImage(P, image);

    imgFile f50("07_try_za_shader_02_vertex_FillBorder.ppx");
    f50.save_p6(image, width, height);
    std::cout << "07_try_za_shader_02_vertex_FillBorder.ppx" << std::endl;

    //***********************************************************//

    P.deletePixelBuffer();
    P.createPixelBuffer(height, width);

    auto tr3 = triangle({ 10, 170 }, { 25, 10 }, { 170, 170 }, color::Navy);
    P.updatePixelBuffer(tr3);

    P.applyShader(shaders::vertex::fill3color, *v, P.getW(), P.getH());

    writePixelBufferToImage(P, image);

    imgFile f60("08_try_za_shader_03_vertex_FillColor.ppx");
    f60.save_p6(image, width, height);
    std::cout << "08_try_za_shader_03_vertex_FillColor.ppx" << std::endl;

    //***********************************************************//
    /*
        P.deletePixelBuffer();
        P.createPixelBuffer(height, width);

        auto tr4 = triangle({ 10, 170 }, { 25, 10 }, { 170, 170 }, color::Navy);
        P.updatePixelBuffer(tr4);

        P.applyShader(shaders::vertex::fill3L, *v, P.getW(), P.getH());

        writePixelBufferToImage(P, image);

        imgFile f70("09_try_za_shader_03_vertex_FillL.ppx");
        f70.save_p6(image, width, height);
        std::cout << "09_try_za_shader_03_vertex_FillL.ppx" << std::endl;
    */
    //***********************************************************//

    return 0;
}
