/*
 * primitive.cpp
 *
 *  Created on: Sep 29, 2018
 *      Author: vito
 */

#include "../include/primitiverender.h"

#include "../include/vitobasicdrawingtypes.hxx"
namespace vitoBasicDrawing
{

primitive_render::primitive_render()
    : m_(new primitive_implementation())
{
}

primitive_render::~primitive_render()
{
    deletePixelBuffer();
    m_.release();
}

/// TODO: add parameter for initail color
/// width is always > height; pixelBuffer has no space orientation
int_least8_t primitive_render::createPixelBuffer(distance height,
                                                 distance width) noexcept
{

    if (m_)
    {
        m_->createPixelBuffer(height, width);
        return 1;
    }
    else
    {
        return 0;
    }
}

std::vector<pixel>* primitive_render::readPixelBuffer() noexcept
{
    return m_->readPixelBuffer();
}

int_least8_t primitive_render::updatePixelBuffer(pixalizable& primitive_)
{
    if (m_)
    {
        return m_->updatePixelBuffer(primitive_);
        return 1;
    }
    else
    {
        return 0;
    }
}

int_least8_t primitive_render::deletePixelBuffer() noexcept
{
    if (m_)
    {
        return m_->deletePixelBuffer();
    }
    else
    {
        return 0;
    }
}

distance primitive_render::getH() const
{
    return m_->getH();
}

distance primitive_render::getW() const
{
    return m_->getW();
}
int_least8_t primitive_render::applyShader(primitiveAlgorytm g, pixalizable& p)
{
    return m_->applyShader(g, p);
}
/*
int_least8_t primitive_render::applyShader(pixelAlgorytm       p,
                                           std::vector<point>& borders)
{
    return m_->applyShader(p, borders);
}
*/
int_least8_t primitive_render::applyShader(vertexAlgorytm        v,
                                           std::vector<pvertex>& vv, distance w,
                                           distance h)
{
    return m_->applyShader(v, vv, w, h);
}
/*
template <size_t N>
void writePixelBufferToImage(const primitive&      P,
                             std::array<pixel, N>& image) noexcept
{
    P.m_->writePixelBufToImage<N>(image);
}
*/
} /* namespace vitoBasicDrawing */
