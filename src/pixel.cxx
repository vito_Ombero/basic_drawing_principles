/*
 * pixel.cxx
 *
 *  Created on: Oct 6, 2018
 *      Author: vito
 */
#include "/home/vito/eclipseCosmos/cppOpenGlws/basic_drawing_principles/include/pixel.hxx"
namespace vitoBasicDrawing
{
pixel::pixel()
{
    r = 255;
    g = 255;
    b = 255;
}

pixel::pixel(byte val)
{
    r = val;
    g = val;
    b = val;
}

pixel::pixel(byte r_, byte g_, byte b_)
{
    r = r_;
    g = g_;
    b = b_;
}

/// need for using color map
bool pixel::operator==(const pixel& b) const
{
    return (this->r == b.r && this->g == b.g && this->b == b.b);
}
/// need for shaders
bool pixel::operator!=(const pixel& b) const
{
    return (this->r != b.r && this->g != b.g && this->b != b.b);
}

/// TODO: refactor&uncomment
/// reinterpret_cast<> wrapper
/*
template <typename T>
color color_cast<T>(T val)
{
    return reinterpret_cast<byte>(val) % 28;
};
*/
/*
byte closer_d128(byte val)
{
    byte r;
    if (val <= 64)
        r = 0;
    else
    {
        if (val <= 128)
            r = 128;
        else
        {
            if (val <= 192)
                r = 128;
            else
                r = 255;
        }
    }
    return r;
}
*/

} // namespace vitoBasicDrawing
