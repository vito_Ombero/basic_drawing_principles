/*
 * pixalizable.cpp
 *
 *  Created on: Oct 6, 2018
 *      Author: vito
 */

#include <map>

#include "/home/vito/eclipseCosmos/cppOpenGlws/basic_drawing_principles/include/pixalizable.h"
#include "/home/vito/eclipseCosmos/cppOpenGlws/basic_drawing_principles/include/pixel.hxx"
#include "/home/vito/eclipseCosmos/cppOpenGlws/basic_drawing_principles/include/vitobasicdrawingtypes.hxx"
namespace vitoBasicDrawing
{

void pixalizable::setColor(pixel p)
{
    if (color_buf != nullptr)
    {
        delete color_buf;
    }
    color_buf = new pixel(p);
    // TODO: make implicit autodetection if color has name
    setColor(color::Unnamed);
}

pixel pixalizable::getColor_() const
{
    if (color_buf != nullptr)
    {
        return *color_buf;
    }
    else
    {
        return colorMap.find(getColor())->second;
    }
}

} /* namespace vitoBasicDrawing */
