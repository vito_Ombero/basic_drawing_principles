/*
 * algorytms.cxx
 *
 *  Created on: Oct 5, 2018
 *      Author: vito
 */

#include <cmath>
#include <vector>

#include "/home/vito/eclipseCosmos/cppOpenGlws/basic_drawing_principles/include/algorytms.hxx"

namespace vitoBasicDrawing
{

void line_positions(point p1, point p2, std::vector<point>& positions_output,
                    distance& pixels_count_output)
{
    coord x1 = p1.x;
    coord y1 = p1.y;
    coord x2 = p2.x;
    coord y2 = p2.y;

    auto lineX = [&](coord x1, coord y1, coord x2, coord y2) {
        signed_coord dx = x2 - x1;
        signed_coord dy = y2 - y1;
        coord        yi = 1;
        if (dy < 0)
        {
            yi = -1;
            dy = -dy;
        }
        signed_coord D = 2 * dy - dx;
        coord        y = y1;

        for (coord x = x1; x <= x2; ++x)
        {
            positions_output.push_back({ x, y });
            pixels_count_output++;
            if (D > 0)
            {
                y += yi;
                D -= 2 * dx;
            }
            D += 2 * dy;
        }
    };

    auto lineY = [&](coord x1, coord y1, coord x2, coord y2) {
        signed_coord dx = x2 - x1;
        signed_coord dy = y2 - y1;
        coord        xi = 1;
        if (dx < 0)
        {
            xi = -1;
            dx = -dx;
        }
        signed_coord D = 2 * dx - dy;
        coord        x = x1;

        for (coord y = y1; y <= y2; ++y)
        {
            positions_output.push_back({ x, y });
            pixels_count_output++;
            if (D > 0)
            {
                x += xi;
                D -= 2 * dy;
            }
            D += 2 * dx;
        }
    };

    if (abs(y2 - y1) < abs(x2 - x1))
    {
        if (x1 > x2)
        {
            lineX(x2, y2, x1, y1);
        }
        else
        {
            lineX(x1, y1, x2, y2);
        }
    }
    else
    {
        if (y1 > y2)
        {
            lineY(x2, y2, x1, y1);
        }
        else
        {
            lineY(x1, y1, x2, y2);
        }
    }
}

struct zpoint
{
    float x;
    float y;

    point toPoint() { return point{ (coord)round(x), (coord)round(y) }; }
};

struct vector
{
    shs_distance x;
    shs_distance y;

    vector()
    {
        x = 0;
        y = 0;
    }
    vector(shs_distance a, shs_distance b)
    {
        x = a;
        y = b;
    }

    vector operator+(const vector& v) const { return vector(x + v.x, y + v.y); }
    float  getAngle() { return atan2(y, x); }
};

distance distanceToPoint(point from, point to)
{
    return distance(sqrt(pow((from.x - to.x), 2) + pow((from.y - to.y), 2)));
}
/*
point bigLegEndpoint(point rightAnglePoint, point smallLegEndPoint)
{
    float half_a = sqrt(pow((rightAnglePoint.x - smallLegEndPoint.x), 2) +
                        pow((rightAnglePoint.y - smallLegEndPoint.y), 2));

    return zpoint{ rightAnglePoint.x + sqrt(3)*half_a*std::sin(), 1
}.toPoint();
};

point heightEndEQTriangle(point base_l, point base_r)
{
    zpoint m{ (base_l.x + base_r.x) / 2, (base_l.y + base_r.y) / 2 };

    return bigLegEndpoint(m.toPoint(), base_r);
};
*/

void dot_position(point d, std::vector<point>& v_output)
{
    v_output.push_back(d);
}

void cirle_positions(point c, distance r, std::vector<point>& v_output,
                     distance& pixel_count_output)
{
    signed_coord x     = 0;
    signed_coord y     = r;
    signed_coord delta = 1 - 2 * r;
    signed_coord error = 0;
    while (y >= 0)
    {
        dot_position(point{ sc(c.x + sc(x)), sc(c.y + sc(y)) }, v_output);
        dot_position(point{ sc(c.x + sc(x)), sc(c.y - sc(y)) }, v_output);
        dot_position(point{ sc(c.x - sc(x)), sc(c.y + sc(y)) }, v_output);
        dot_position(point{ sc(c.x - sc(x)), sc(c.y - sc(y)) }, v_output);
        pixel_count_output += 4;
        error = 2 * (delta + y) - 1;
        if ((delta < 0) && (error <= 0))
        {
            delta += 2 * ++x + 1;
            continue;
        }
        if ((delta > 0) && (error > 0))
        {
            delta -= 2 * --y + 1;
            continue;
        }
        delta += 2 * (++x - y--);
    }
}

inline coord sc(signed_coord s)
{
    return static_cast<coord>(s);
}

} // namespace vitoBasicDrawing
