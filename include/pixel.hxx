/*
 * pixel.hxx
 *
 *  Created on: Sep 29, 2018
 *      Author: vito
 */

#ifndef PIXEL_HXX_
#define PIXEL_HXX_

#include "/home/vito/eclipseCosmos/cppOpenGlws/basic_drawing_principles/include/vitobasicdrawingtypes.hxx"
namespace vitoBasicDrawing
{

struct pixel final
{
    byte r;
    byte g;
    byte b;

    pixel();

    pixel(byte val);

    pixel(byte r_, byte g_, byte b_);

    /// need for using color map
    bool operator==(const pixel& b) const;
    /// need for shaders
    bool operator!=(const pixel& b) const;
};

static std::map<color, pixel> colorMap = {
    { color::Black, pixel(0, 0, 0) },
    { color::Navy, pixel(0, 0, 128) },
    { color::Blue, pixel(0, 0, 255) },
    { color::Green, pixel(0, 128, 0) },
    { color::Teal, pixel(0, 128, 128) },
    { color::Blue_light, pixel(0, 128, 255) },
    { color::Lime, pixel{ 0, 255, 0 } },
    { color::Green_springB, pixel{ 0, 255, 128 } },
    { color::Cyan, pixel{ 0, 255, 255 } },
    { color::Indigo, pixel{ 75, 0, 130 } },
    { color::Maroon, pixel{ 128, 0, 0 } },
    { color::Purple, pixel{ 128, 0, 128 } },
    { color::Purple_blue, pixel{ 128, 0, 255 } },
    { color::Olive, pixel{ 128, 128, 0 } },
    { color::Gray, pixel{ 128, 128, 128 } },
    { color::Sky_blue_dark, pixel{ 128, 128, 255 } },
    { color::Silver, pixel{ 192, 192, 192 } },
    { color::Violet, pixel{ 238, 130, 238 } },
    { color::Red, pixel{ 255, 0, 0 } },
    { color::Fuchsia_dark, pixel{ 255, 0, 128 } },
    { color::Magenta, pixel{ 255, 0, 255 } },
    { color::DarkOrange1B, pixel{ 255, 128, 0 } },
    { color::Orange, pixel{ 255, 165, 0 } },
    { color::Coral_lightd, pixel{ 255, 128, 128 } },
    { color::Orchid1_l, pixel{ 255, 128, 255 } },
    { color::Yellow, pixel{ 255, 255, 0 } },
    { color::Khaki1_l, pixel{ 255, 255, 128 } },
    { color::White, pixel{ 255, 255, 255 } }
};

typedef std::map<color, pixel> cmap;

} // namespace vitoBasicDrawing
#endif /* PIXEL_HXX_ */
