/*
 * primitive_implementation.hxx
 *
 *  Created on: Sep 29, 2018
 *      Author: vito
 */

#ifndef PRIMITIVE_IMPLEMENTATION_HXX_
#define PRIMITIVE_IMPLEMENTATION_HXX_

#include <cassert>
#include <memory>
#include <vector>

#include "/home/vito/eclipseCosmos/cppOpenGlws/basic_drawing_principles/include/pixalizable.h"
#include "/home/vito/eclipseCosmos/cppOpenGlws/basic_drawing_principles/include/pixel.hxx"
#include "/home/vito/eclipseCosmos/cppOpenGlws/basic_drawing_principles/include/vitobasicdrawingtypes.hxx"
namespace vitoBasicDrawing
{

typedef void (*vertexAlgorytm)(std::vector<pvertex>&, std::vector<pixel>&,
                               distance, distance);
typedef void (*pixelAlgorytm)(std::vector<point>&, std::vector<pixel>&);
typedef void (*primitiveAlgorytm)(vitoBasicDrawing::pixalizable&);
typedef void (*vshader)(vertexAlgorytm);
typedef void (*pshader)(pixelAlgorytm);
typedef void (*gshader)(primitiveAlgorytm, std::vector<pixel>&);

struct primitive_implementation final
{
    typedef primitive_implementation pi;

    // primitive_implementation(const pi&) = delete;
    // primitive_implementation(pi&&)      = delete;
    pi& operator=(const pi&) = delete;
    pi& operator=(const pi&&) = delete;

private:
    std::vector<pixel>* pixelBuffer_ptr;
    distance            h;
    distance            w;

    inline coord place(coord x, coord y, coord dw)
    {
        coord temp = x + y * dw;
        if (temp < h * w)
            return temp;
        else
            return 0;
    }

public:
    // TODO: make it template and remove hardcorded DISTANCE_MAX check, add 3rd
    // parameter for check
    int_least8_t createPixelBuffer(distance height, distance width) noexcept
    {
        assert(height > 0 && width > 0);

        if (height < width)
        {
            h = width;
            w = height;
        }
        else
        {
            h = height;
            w = width;
        }
        if (height > DISTANCE_MAX / width)
        {
            return 0;
        }
        else
        {
            pixelBuffer_ptr = new std::vector<pixel>;

            for (distance i = 0; i < (h - 1) * (w - 1); ++i)
            {
                pixelBuffer_ptr->push_back(pixel());
            }

            return 1;
        }
    };

    // TODO: add parameters read_from read_to
    std::vector<pixel>* readPixelBuffer() noexcept { return pixelBuffer_ptr; };
    int_least8_t        updatePixelBuffer(pixalizable& primitive_)
    {
        if (pixelBuffer_ptr != nullptr)
        {

            assert(primitive_.getRDOP().y < h && primitive_.getRDOP().x < w);

            distance prim_W =
                primitive_.getRDOP().x - primitive_.getLUOP().x + 1;
            assert(prim_W <= w);

            distance prim_H =
                primitive_.getRDOP().y - primitive_.getLUOP().y + 1;
            assert(prim_H <= h);

            auto temp = new std::vector<point>(primitive_.getPixelsTotal());

            assert(temp->size() <= prim_W * prim_H);

            if (primitive_.imageSelf(*temp))
            {
                for (point tp : *temp)
                {
                    pixelBuffer_ptr->at(place(tp.x, tp.y, w)) =
                        colorMap.find(primitive_.getColor())->second;
                }

                delete temp;
                return 1;
            }
            else
            {
                delete temp;
                return 0;
            }
        }
        return 0;
    }
    int_least8_t deletePixelBuffer() noexcept
    {
        if (pixelBuffer_ptr)
        {
            delete pixelBuffer_ptr;
            return 1;
        }
        return 0;
    }
    distance getH() const { return h; }

    distance getW() const { return w; }

    template <size_t N>
    void writePixelBufToImage(std::array<pixel, N>& image) noexcept
    {
        for (long unsigned int i = 0; i < image.size(); ++i)
        {
            image[i] = pixelBuffer_ptr->at(i);
        }
    }

    int_least8_t applyShader(primitiveAlgorytm g, pixalizable& p)
    {
        (g(p));
        return 1;
    }
    /*
        int_least8_t applyShader(pixelAlgorytm p, std::vector<point>& borders)
        {
            (p(borders, *pixelBuffer_ptr));
            return 1;
        }
        */
    int_least8_t applyShader(vertexAlgorytm v, std::vector<pvertex>& vv,
                             distance w, distance h)
    {
        (v(vv, *pixelBuffer_ptr, w, h));
        return 0;
    }
};

} // namespace vitoBasicDrawing

#endif /* PRIMITIVE_IMPLEMENTATION_HXX_ */
