/*
 * pixalizable.h
 *
 *  Created on: Oct 6, 2018
 *      Author: vito
 */

#ifndef PIXALIZABLE_H_
#define PIXALIZABLE_H_

#include <cassert>
#include <map>
#include <memory>
#include <vector>

#include "/home/vito/eclipseCosmos/cppOpenGlws/basic_drawing_principles/include/pixel.hxx"
#include "/home/vito/eclipseCosmos/cppOpenGlws/basic_drawing_principles/include/vitobasicdrawingtypes.hxx"

namespace vitoBasicDrawing
{

struct pixalizable
{
public:
    void     setColor(color c) { col = c; };
    color    getColor() const { return col; };
    point    getLUOP() const { return leftUpperOffsetPoint; };
    point    getRDOP() const { return rightDownerOffsetPoint; };
    distance getPixelsTotal() const { return pixelsTotal; }

    // TODO: no imageSelf - refactor for shaders draw
    virtual int_least8_t imageSelf(std::vector<point>& positions) noexcept = 0;
    virtual void         setOffset(point luop, point rdop) noexcept        = 0;

    void  setColor(pixel p);
    pixel getColor_() const;

private:
    color    col       = color::White;
    pixel*   color_buf = nullptr;
    distance pixelsTotal;
    // TODO: make private list<vertex> + methods to implement by children

protected:
    pixalizable()
        : pixelsTotal(0)
    {
        rightDownerOffsetPoint = { 0, 0 };
        leftUpperOffsetPoint   = { 0, 0 };
    };
    virtual ~pixalizable() { delete color_buf; };
    point leftUpperOffsetPoint;
    point rightDownerOffsetPoint;
    void  setPixelsTotal(distance n) { pixelsTotal = n; };
};

} /* namespace vitoBasicDrawing */

#endif /* PIXALIZABLE_H_ */
