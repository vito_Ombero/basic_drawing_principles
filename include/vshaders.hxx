/*
 * vshaders.hxx
 *
 *  Created on: Oct 6, 2018
 *      Author: vito
 */

#ifndef VSHADERS_HXX_
#define VSHADERS_HXX_

#include <cassert>

#include "/home/vito/eclipseCosmos/cppOpenGlws/basic_drawing_principles/include/vitobasicdrawingtypes.hxx"

#include "/home/vito/eclipseCosmos/cppOpenGlws/basic_drawing_principles/include/pixalizable.h"
#include "/home/vito/eclipseCosmos/cppOpenGlws/basic_drawing_principles/include/pixel.hxx"

#include "/home/vito/eclipseCosmos/cppOpenGlws/basic_drawing_principles/include/algorytms.hxx"

namespace vitoBasicDrawing
{
/// TODO: wrong idea and model of shader
// smust be something like
// std::vector<var> vshader(std::vector<attributes>)
// struct atribute { float _1;float _2;float _3;float _4;}

namespace shaders
{

namespace vertex
{
// TODO:  compare rendering time
// expecting LEFT MIDDLE RIGHT
void fill3border(std::vector<pvertex>& v /*in*/,
                 std::vector<pixel>& pixbuf /*out*/, distance w, distance h)
{
    v.shrink_to_fit();
    assert(v.size() == 3);

    pixel color_probe = pixbuf[vitoBasicDrawing::placeWH(v[0].x, v[0].y, w, h)];

    point max{ vitoBasicDrawing::pointsCollectCoord(v[0], v[1], v[2], true) };
    point min{ vitoBasicDrawing::pointsCollectCoord(v[0], v[1], v[2], false) };

    // detect_case
    auto     LD = new std::vector<point>;
    auto     RD = new std::vector<point>;
    distance countL{ 0 }; // todo: make new overload
    distance countR{ 0 };

    // filling : y_start > y_end
    if (v[0].y == v[2].y && v[1].y == min.y)
    {
        vitoBasicDrawing::line_positions(v[0], v[1], *LD, countL);
        vitoBasicDrawing::line_positions(v[2], v[1], *RD, countR);
    }
    else if (v[0].y == v[2].y && v[1].y == max.y)
    {
        vitoBasicDrawing::line_positions(v[0], v[2], *LD, countL);
        vitoBasicDrawing::line_positions(v[1], v[2], *RD, countR);
    }
    else if (v[0].x == min.x && v[1].x == v[2].x)
    { // undefined direction
        if (v[1].y < v[2].y)
        {
            vitoBasicDrawing::line_positions(v[2], v[0], *LD, countL);
            vitoBasicDrawing::line_positions(v[0], v[1], *LD, countL);
            vitoBasicDrawing::line_positions(v[2], v[1], *RD, countR);
        }
        else
        {
            vitoBasicDrawing::line_positions(v[1], v[0], *LD, countL);
            vitoBasicDrawing::line_positions(v[0], v[2], *LD, countL);
            vitoBasicDrawing::line_positions(v[1], v[2], *RD, countR);
        }
    }
    else if (v[2].x == max.x && v[0].x == v[1].x)
    { // undefined direction
        if (v[1].y > v[0].y)
        {
            vitoBasicDrawing::line_positions(v[1], v[0], *LD, countL);
            vitoBasicDrawing::line_positions(v[1], v[2], *RD, countR);
            vitoBasicDrawing::line_positions(v[2], v[0], *RD, countR);
        }
        else
        {
            vitoBasicDrawing::line_positions(v[0], v[1], *LD, countL);
            vitoBasicDrawing::line_positions(v[0], v[2], *RD, countR);
            vitoBasicDrawing::line_positions(v[2], v[1], *RD, countR);
        }
    }
    else if (v[2].y == max.y)
    {
        vitoBasicDrawing::line_positions(v[2], v[0], *LD, countL);
        vitoBasicDrawing::line_positions(v[0], v[1], *LD, countL);
        vitoBasicDrawing::line_positions(v[2], v[1], *RD, countR);
    }
    else if (v[0].y == max.y)
    {
        vitoBasicDrawing::line_positions(v[0], v[1], *LD, countL);
        vitoBasicDrawing::line_positions(v[0], v[2], *RD, countR);
        vitoBasicDrawing::line_positions(v[2], v[1], *RD, countR);
    };

    distance            count;
    std::vector<point>* DD;

    if (countL < countR)
    {
        count = countL;
        DD    = LD;
    }
    else
    {
        count = countR;
        DD    = RD;
    };

    for (coord i = count; i > 0; --i)
    {
        for (coord x = (*LD)[i].x + 1; x < (*RD)[i].x; ++x)
        {
            pixbuf[vitoBasicDrawing::placeWH(x, (*DD)[i].y, w, h)] =
                color_probe;
        }
    }

    delete LD;
    delete RD;
    delete DD;
}

// expecting LEFT MIDDLE RIGHT

void fill3color(std::vector<pvertex>& v /*in*/,
                std::vector<pixel>& pixbuf /*out*/, distance w, distance h)
{
    v.shrink_to_fit();
    assert(v.size() == 3);

    pixel color_probe_b =
        pixbuf[vitoBasicDrawing::placeWH(v[0].x, v[0].y, w, h)];

    point max{ vitoBasicDrawing::pointsCollectCoord(v[0], v[1], v[2], true) };
    point min{ vitoBasicDrawing::pointsCollectCoord(v[0], v[1], v[2], false) };
    coord mid_x = ceil((max.x - min.x) / 2);
    coord mid_y = ceil((max.y - min.y) / 2);
    point mid{ mid_x, mid_y };
    // pixel color_probe_a = pixbuf[vitoBasicDrawing::placeWH(mid.x, mid.y, w,
    // h)];

    for (coord y = mid.y; y <= max.y; ++y)
    {
        coord x   = mid.x;
        bool  cnt = true;
        while (cnt)
        {
            if (pixbuf[vitoBasicDrawing::placeWH(x, y, w, h)] != color_probe_b)
            {
                pixbuf[vitoBasicDrawing::placeWH(x, y, w, h)] = color_probe_b;
            }
            else
                cnt = false;
            ++x;
        }
        cnt = true;
        while (cnt)
        {
            if (pixbuf[vitoBasicDrawing::placeWH(x, y, w, h)] != color_probe_b)
            {
                pixbuf[vitoBasicDrawing::placeWH(x, y, w, h)] = color_probe_b;
            }
            else
                cnt = false;
            ++x;
        }
    }

    for (coord y = mid.y; y >= min.y; y--)
    {
        coord x   = mid.x;
        bool  cnt = true;
        while (cnt)
        {
            if (pixbuf[vitoBasicDrawing::placeWH(x, y, w, h)] != color_probe_b)
            {
                pixbuf[vitoBasicDrawing::placeWH(x, y, w, h)] = color_probe_b;
            }
            else
                cnt = false;
            ++x;
        }
        cnt = true;
        while (cnt)
        {
            if (pixbuf[vitoBasicDrawing::placeWH(x, y, w, h)] != color_probe_b)
            {
                pixbuf[vitoBasicDrawing::placeWH(x, y, w, h)] = color_probe_b;
            }
            else
                cnt = false;
            ++x;
        }
    }
}
/*
void fill3L(std::vector<pvertex>& v , std::vector<pixel>& pixbuf ,
            distance w, distance h)
{
    v.shrink_to_fit();
    assert(v.size() == 3);

    pixel color_probe = pixbuf[vitoBasicDrawing::placeWH(v[0].x, v[0].y, w, h)];

    auto D1 = new std::vector<point>;
    auto D2 = new std::vector<point>;
    auto D3 = new std::vector<point>;

    auto S = new std::vector<point>;

    distance count{ 0 };
    distance c1{ 0 };
    distance c2{ 0 };
    distance c3{ 0 };

    vitoBasicDrawing::line_positions(v[0], v[1], *D1, c1);

    vitoBasicDrawing::line_positions(v[0], v[2], *D2, c2);

    vitoBasicDrawing::line_positions(v[2], v[1], *D3, c3);

    point max{ vitoBasicDrawing::pointsCollectCoord(v[0], v[1], v[2], true) };
    point min{ vitoBasicDrawing::pointsCollectCoord(v[0], v[1], v[2], false) };

    std::vector<point>* X;
    std::vector<point>* Y;
    distance            cn{ 0 };

    // filling : y_start > y_end
    if (v[0].y == v[2].y && v[1].y == min.y)
    {
        // case 1
    }
    else if (v[0].y == v[2].y && v[1].y == max.y)
    {
        // case 2
    }
    else if (v[0].x == min.x && v[1].x == v[2].x)
    { // undefined direction
        if (v[1].y < v[2].y)
        {
            // case 3-1
        }
        else
        {
            // case 3-2
        }
    }
    else if (v[2].x == max.x && v[0].x == v[1].x)
    { // undefined direction
        if (v[1].y > v[0].y)
        {
            // case 4-1
        }
        else
        {
            // case 4-2
        }
    }
    else if (v[2].y == max.y)
    {
        // case 5
    }
    else if (v[0].y == max.y)
    {
        // case 6
    };

    coord* k;
    coord  ki, kj;
    if (*X->size() < *Y->size())
        k = &ki;
    else
        k = &kj;

    for (ki = (*X)[0].x, kj = (*Y)[0].y; ki <= (*X)[cn].x && kj <= (*Y)[cn].y;
         ++ki, ++kj)
    {

        vitoBasicDrawing::line_positions((*X)[*k], (*Y)[*k], *S, count);
    }

    for (coord i = 0; i < count; ++i)
    {
        pixbuf[vitoBasicDrawing::placeWH((*S)[i].x, (*S)[i].y, w, h)] =
            color_probe;
    }

    delete D1;
    delete D2;
    delete D3;
    delete S;
}
*/
} // namespace vertex

} // namespace shaders
} // namespace vitoBasicDrawing

#endif /* VSHADERS_HXX_ */
