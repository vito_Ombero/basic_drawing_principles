/*
 * gshaders.hxx
 *
 *  Created on: Oct 6, 2018
 *      Author: vito
 */

#ifndef GSHADERS_HXX_
#define GSHADERS_HXX_

#include <cmath>
#include <vector>

#include "/home/vito/eclipseCosmos/cppOpenGlws/basic_drawing_principles/include/vitobasicdrawingtypes.hxx"

#include "/home/vito/eclipseCosmos/cppOpenGlws/basic_drawing_principles/include/pixalizable.h"
#include "/home/vito/eclipseCosmos/cppOpenGlws/basic_drawing_principles/include/pixel.hxx"

namespace vitoBasicDrawing
{
namespace shaders
{

namespace primitive
{

void invertColor(vitoBasicDrawing::pixalizable& p)
{
    vitoBasicDrawing::pixel was =
        vitoBasicDrawing::colorMap.find(p.getColor())->second;
    was.b = (was.b + 125) % 255;
    was.r = (was.r + 125) % 255;
    was.g = (was.g + 125) % 255;
    p.setColor(was);
}
// TODO: yet primitive shader is just an emulation
} // namespace primitive

} // namespace shaders
} // namespace vitoBasicDrawing
#endif /* GSHADERS_HXX_ */
