/*
 * pshaders.hxx
 *
 *  Created on: Oct 6, 2018
 *      Author: vito
 */

#ifndef PSHADERS_HXX_
#define PSHADERS_HXX_
#include "/home/vito/eclipseCosmos/cppOpenGlws/basic_drawing_principles/include/vitobasicdrawingtypes.hxx"

#include "/home/vito/eclipseCosmos/cppOpenGlws/basic_drawing_principles/include/pixalizable.h"
#include "/home/vito/eclipseCosmos/cppOpenGlws/basic_drawing_principles/include/pixel.hxx"

namespace vitoBasicDrawing
{
namespace shaders
{

namespace pixel
{

void avarageColor(std::vector<point>& borders /*in*/,
                  std::vector<pixel>& pixbuf /*out*/)
{
}

} // namespace pixel

} // namespace shaders
} // namespace vitoBasicDrawing
#endif /* PSHADERS_HXX_ */
