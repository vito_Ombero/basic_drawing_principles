#ifndef vitoBasicDrawingTYPES_HXX_
#define vitoBasicDrawingTYPES_HXX_

#include <map>
#include <vector>

namespace vitoBasicDrawing
{

typedef uint16_t coord, distance;
#define DISTANCE_MAX UINT16_MAX
#define COORD_MAX UINT16_MAX
#define IMAGE_H_OR_W_MAX DISTANCE / 2
typedef int32_t signed_coord, signed_distance;
typedef int16_t shs_coord, shs_distance;
// TODO: wrap this as memeber-field maybe ->>
// https://en.cppreference.com/w/cpp/types/byte
typedef unsigned char byte;
struct point
{
    coord x;
    coord y;
};

typedef point pvertex;

/// rainbow colors: red/orange/yellow/green/blue/indigo/violet
enum color
{
    No,
    Unnamed,
    /// http://shallowsky.com/colormatch/index.php
    Black,         //#000000	(0,0,0)
    Navy,          //#000080	(0,0,128)
    Blue,          //#0000FF	(0,0,255)
    Green,         //#008000	(0,128,0)
    Teal,          //#008080	(0,128,128)
    Blue_light,    //####### 	(0,128,255)
    Lime,          //#00FF00	(0,255,0)
    Green_springB, //#######	(0,255,128) Green_spring_is(0,255,127)
    Cyan,          //#00FFFF	(0,255,255)
    Indigo,        //#######	(75,0,130)
    Maroon,        //#800000	(128,0,0)
    Purple,        //#800000	(128,0,128)
    Purple_blue,   //#800000	(128,0,255)
    Olive,         //#808000	(128,128,0)
    Gray,          //#808080	(128,128,128)
    Sky_blue_dark, //#######	(128,128,255)
    Silver,        //#C0C0C0	(192,192,192)
    Violet,        //####### 	(238,130,238)
    Red,           //#FF0000	(255,0,0)
    Fuchsia_dark,  //#FF0000	(255,0,128)
    Magenta,       //#FF00FF	(255,0,255)
    DarkOrange1B,  //#FF0000	(255,128,0) DarkOrange1_is(255,127,0)
    Orange,        //#######	(255,165,0)
    Coral_lightd,  //#FF0000	(255,128,128)
    Orchid1_l,     //#FF0000	(255,128,255)
    Yellow,        //#FFFF00	(255,255,0)
    Khaki1_l,      //#FFFF00	(255,255,128)
    White,         //#FFFFFF	(255,255,255)
};

/*
/// ignore rainbow colors
cmap::iterator color_match(char r, char g, char b)
{
    assert(r >= 0);
    assert(g >= 0);
    assert(b >= 0);

    std::map<color, pixel>::iterator it =
        std::find_if(colorMap.begin(), colorMap.end(), [&](pixel p) -> bool {
            return (p == pixel(closer_d128(r), closer_d128(g), closer_d128(b)));
        });
    return it;
}

cmap::iterator color_match(byte val)
{
    return color_match(val, val, val);
}
*/

} // namespace vitoBasicDrawing
#endif /* vitoBasicDrawingTYPES_HXX_ */
