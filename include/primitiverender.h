/*
 * primitive.h
 *
 *  Created on: Sep 29, 2018
 *      Author: vito
 */

#ifndef PRIMITIVERENDER_H_
#define PRIMITIVERENDER_H_

#include <iostream>
#include <vector>

#include "/home/vito/eclipseCosmos/cppOpenGlws/basic_drawing_principles/include/vitobasicdrawingtypes.hxx"

#include "/home/vito/eclipseCosmos/cppOpenGlws/basic_drawing_principles/include/pixalizable.h"
#include "/home/vito/eclipseCosmos/cppOpenGlws/basic_drawing_principles/include/primitive_implementation.hxx"

#include "/home/vito/eclipseCosmos/cppOpenGlws/basic_drawing_principles/include/algorytms.hxx"

namespace vitoBasicDrawing
{

typedef void (*vertexAlgorytm)(std::vector<pvertex>&, std::vector<pixel>&,
                               distance, distance);
typedef void (*pixelAlgorytm)(std::vector<point>&, std::vector<pixel>&);
typedef void (*primitiveAlgorytm)(vitoBasicDrawing::pixalizable&);
typedef void (*vshader)(vertexAlgorytm);
typedef void (*pshader)(pixelAlgorytm);
typedef void (*gshader)(primitiveAlgorytm, std::vector<pixel>&);

/// utility elements
struct point;

/// drawable contours
struct dot;
struct line;
struct circle;
struct triangle;

/// render simulation
struct Rtrinagle;
struct Rcircle;

class primitive_render final
{
public:
    primitive_render();
    ~primitive_render();

    /// primitive_render interface
    int_least8_t createPixelBuffer(distance height, distance width) noexcept;
    std::vector<pixel>* readPixelBuffer() noexcept;
    int_least8_t        updatePixelBuffer(pixalizable& primitive_);
    int_least8_t        deletePixelBuffer() noexcept;
    distance            getH() const;
    distance            getW() const;
    int_least8_t        applyShader(primitiveAlgorytm g, pixalizable& p);
    // int_least8_t applyShader(pixelAlgorytm p, std::vector<point>& borders);
    int_least8_t applyShader(vertexAlgorytm v, std::vector<pvertex>& vv,
                             distance w, distance h);

public:
    template <size_t N>
    friend void writePixelBufferToImage(const primitive_render& P,
                                        std::array<pixel, N>&   image) noexcept
    {
        P.m_->writePixelBufToImage<N>(image);
    }

private:
    std::unique_ptr<primitive_implementation> m_;

private:
    primitive_render(const primitive_render&) = delete;
    primitive_render(primitive_render&&)      = delete;
    primitive_render& operator=(const primitive_render&) = delete;
    primitive_render& operator=(const primitive_render&&) = delete;
    void*             operator new(std::size_t, void*)    = delete;
};

struct dot final : public pixalizable
{
    /// TODO: make vertex vector maybe
private:
    point p;

public:
    /// pixe interface implementation

    int_least8_t imageSelf(std::vector<point>& positions) noexcept override
    {
        if (getColor() != color::No)
        {
            dot_position(p, positions);
            setPixelsTotal(1);
            return 1;
        }
        else
        {
            return 0;
        }
    };

    void setOffset(point l, point r) noexcept override
    {
        rightDownerOffsetPoint = { l.x, l.y };
        leftUpperOffsetPoint   = { r.x, r.y };
    }

    // explicit consturtor requireds
    dot()
        : p({ 0, 0 })
    {
        setColor(color::Black);
        setOffset({ 0, 0 }, { 0, 0 });
    }
    dot(coord x, coord y)
        : p({ x, y })
    {
        setColor(color::Black);
        setOffset(p, p);
    }
    dot(point p_, color c)
        : p(p_)
    {
        setColor(c);
        setOffset(p, p);
    }
};

struct line final : public pixalizable
{
private:
    point p1;
    point p2;

public:
    line()
        : p1({ 0, 0 })
        , p2({ 1, 1 })
    {
        setColor(color::Black);
        setOffset(p1, p2);
    };

    line(point p1, point p2)
        : p1(p1)
        , p2(p2)
    {
        setColor(color::Black);
        setOffset(p1, p2);
    };

    line(point p1, point p2, color c)
        : p1(p1)
        , p2(p2)
    {
        setColor(c);
        setOffset(p1, p2);
    };

    void setOffset(point l, point r) noexcept override
    {
        coord lx{ l.x };
        coord ly{ l.y };
        coord hx{ r.x };
        coord hy{ r.y };
        if (lx > hx)
        {
            lx = r.x;
            hx = l.x;
        }
        if (ly > hy)
        {
            ly = r.y;
            hy = l.x;
        }
        leftUpperOffsetPoint   = { lx, ly };
        rightDownerOffsetPoint = { hx, hy };
    }

    int_least8_t imageSelf(std::vector<point>& positions) noexcept override
    {
        if (getColor() != color::No)
        {
            distance count{ 0 };
            line_positions(p1, p2, positions, count);
            setPixelsTotal(count);
            positions.shrink_to_fit();
            return 1;
        }
        else
        {
            return 0;
        }
    };
};

struct triangle final : public pixalizable
{
private:
    point a;
    point b;
    point c;

public:
    triangle()
        : a({ 0, 0 })
        , b({ 2, 0 })
        , c({ 0, 2 })
    {
        setColor(color::Black);
        setOffset(a, { 2, 2 });
    };

    triangle(point a, point b, point c)
        : a(a)
        , b(b)
        , c(c)
    {
        setColor(color::Black);

        setOffset(pointsCollectCoord(a, b, c, false),
                  pointsCollectCoord(a, b, c, true));
    };

    triangle(point a, point b, point c, color co)
        : triangle(a, b, c)
    {
        setColor(co);
    };

    void setOffset(point l, point r) noexcept override
    {
        leftUpperOffsetPoint   = l;
        rightDownerOffsetPoint = r;
    }

    int_least8_t imageSelf(std::vector<point>& positions) noexcept override
    {
        if (getColor() != color::No)
        {
            distance count{ 0 };
            line_positions(c, a, positions, count);
            line_positions(a, b, positions, count);
            line_positions(b, c, positions, count);

            setPixelsTotal(count);
            positions.shrink_to_fit();
            return 1;
        }
        else
        {
            return 0;
        }
    };
};

struct circle final : public pixalizable
{
private:
    point     c;
    point     l;
    distance* r_buf = nullptr;

public:
    circle()
        : c({ 3, 3 })
        , l({ 1, 1 })
    {
        setColor(color::Black);
        setOffset(c, l);
    };

    circle(point c, point l, color co = color::Black)
        : c(c)
        , l(l)
    {
        setColor(co);
        setOffset(c, l);
    };

    circle(point c, distance r, color co = color::Black)
        : c(c)
    {
        distance d   = (r == 0) ? 1 : r;
        coord    one = c.x + d - 1;
        coord    two = c.y + d - 1;
        r_buf        = new distance(r);
        l            = point{ one, two };
        setColor(co);
    };

    void setOffset(point center, point len) noexcept override
    {
        distance d = (r_buf == nullptr) ? distanceToPoint(center, len) : *r_buf;
        assert(center.x + 1 - d >= 0);
        assert(center.y + 1 - d >= 0);
        coord q                = center.x - d + 1;
        coord w                = center.y - d + 1;
        coord e                = center.x + d - 1;
        coord r                = center.y + d - 1;
        leftUpperOffsetPoint   = point{ q, w };
        rightDownerOffsetPoint = point{ e, r };
    }

    int_least8_t imageSelf(std::vector<point>& positions) noexcept override
    {
        if (getColor() != color::No)
        {
            distance count{ 0 };
            distance d = (r_buf == nullptr) ? distanceToPoint(c, l) : *r_buf;
            cirle_positions(c, d, positions, count);
            setPixelsTotal(count);
            positions.shrink_to_fit();
            return 1;
        }
        else
        {
            return 0;
        }
    };
};

} /* namespace vitoBasicDrawing */

#endif /* PRIMITIVERENDER_H_ */
