/*
 * algorytms.hxx
 *
 *  Created on: Oct 5, 2018
 *      Author: vito
 */

#ifndef ALGORYTMS_HXX_
#define ALGORYTMS_HXX_

#include "/home/vito/eclipseCosmos/cppOpenGlws/basic_drawing_principles/include/vitobasicdrawingtypes.hxx"

namespace vitoBasicDrawing
{

inline point ipoint(coord x, coord y, bool inv)
{
    if (inv)
    {
        return point{ y, x };
    }
    else
    {
        return point{ x, y };
    }
}

inline bool icondition(coord x, coord v, bool lesserEq)
{
    if (lesserEq)
        return x <= v;
    else
        return x >= v;
}

inline point pointsCollectCoord(point a, point b, point c, bool max /*or min?*/)
{
    coord lu1{ a.x }, lu2{ a.y };
    if (max)
    {
        lu1 = b.x > lu1 ? b.x : lu1;
        lu1 = c.x > lu1 ? c.x : lu1;
        lu2 = b.y > lu1 ? b.y : lu2;
        lu2 = c.y > lu1 ? c.y : lu2;
    }
    else
    {
        lu1 = b.x < lu1 ? b.x : lu1;
        lu1 = c.x < lu1 ? c.x : lu1;
        lu2 = b.y < lu1 ? b.y : lu2;
        lu2 = c.y < lu1 ? c.y : lu2;
    }

    return point{ lu1, lu2 };
}

inline coord sc(signed_coord s);

void  line_positions(point p1, point p2, std::vector<point>& positions_output,
                     distance& pixels_count_output);
point heightEndTriangle(point base_left, point base_right);
void  dot_position(point d, std::vector<point>& v_output);
distance     distanceToPoint(point from, point to);
void         cirle_positions(point c, distance r, std::vector<point>& v_output,
                             distance& pixel_count_output);
inline coord placeWH(coord x, coord y, distance w, distance h)
{
    coord temp = x + y * w;
    if (temp < h * w)
        return temp;
    else
        return 0;
}
} // namespace vitoBasicDrawing

#endif /* ALGORYTMS_HXX_ */
